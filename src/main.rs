use clap::{App, Arg};
use csv;
use serde::{Deserialize, Serialize};
use std::error::Error;

#[derive(Debug, Deserialize, Serialize)]
struct Character {
    name: String,
    title: String,
    race: String,
    birthday: String,
    gender: String,
    weapon: String,
    domain: String,
}

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("LOTR Character Search Tool")
        .version("0.1.0")
        .author("Your Name")
        .about("A command-line tool for querying LOTR character data")
        .arg(Arg::with_name("file")
            .help("Sets the input CSV file to use")
            .required(true)
            .index(1))
        .arg(Arg::with_name("name")
            .help("Filter by character name")
            .short("n")
            .long("name")
            .takes_value(true))
        .arg(Arg::with_name("race")
            .help("Filter by character race")
            .short("r")
            .long("race")
            .takes_value(true))
        .get_matches();

    let file_path = matches.value_of("file").unwrap();
    let name_filter = matches.value_of("name");
    let race_filter = matches.value_of("race");

    let records = process_csv(file_path)?;
    let filtered_records = filter_records(records, name_filter, race_filter)?;

    print_records(filtered_records);

    Ok(())
}

fn process_csv(file_path: &str) -> Result<Vec<Character>, Box<dyn Error>> {
    let mut rdr = csv::Reader::from_path(file_path)?;
    rdr.deserialize()
        .map(|result| result.map_err(|e| e.into()))
        .collect()
}

fn filter_records(records: Vec<Character>, name_filter: Option<&str>, race_filter: Option<&str>) -> Result<Vec<Character>, Box<dyn Error>> {
    let filtered_records = records.into_iter().filter(|record| {
        let pass_name = match name_filter {
            Some(name) => record.name.to_lowercase().contains(&name.to_lowercase()),
            None => true,
        };

        let pass_race = match race_filter {
            Some(race) => record.race.to_lowercase().contains(&race.to_lowercase()),
            None => true,
        };

        pass_name && pass_race
    }).collect();
    Ok(filtered_records)
}

fn print_records(records: Vec<Character>) {
    for record in records {
        println!("{:?}", record);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // 测试数据创建函数
    fn create_test_characters() -> Vec<Character> {
        vec![
            Character {
                name: "Frodo Baggins".to_string(),
                title: "Ring Bearer".to_string(),
                race: "Hobbit".to_string(),
                birthday: "22 September".to_string(),
                gender: "Male".to_string(),
                weapon: "Sting".to_string(),
                domain: "Shire".to_string(),
            },
            Character {
                name: "Legolas".to_string(),
                title: "Prince of the Woodland Realm".to_string(),
                race: "Elf".to_string(),
                birthday: "".to_string(),
                gender: "Male".to_string(),
                weapon: "Bow and Arrows".to_string(),
                domain: "Woodland Realm".to_string(),
            },
            Character {
                name: "Gimli".to_string(),
                title: "Son of Glóin".to_string(),
                race: "Dwarf".to_string(),
                birthday: "".to_string(),
                gender: "Male".to_string(),
                weapon: "Axe".to_string(),
                domain: "Glittering Caves".to_string(),
            }
        ]
    }

    #[test]
    fn test_filter_by_name() {
        let characters = create_test_characters();
        let filtered = filter_records(characters, Some("Frodo"), None).unwrap();
        assert_eq!(filtered.len(), 1);
        assert_eq!(filtered[0].name, "Frodo Baggins");
    }

    #[test]
    fn test_filter_by_race() {
        let characters = create_test_characters();
        let filtered = filter_records(characters, None, Some("Hobbit")).unwrap();
        assert_eq!(filtered.len(), 1);
        assert_eq!(filtered[0].race, "Hobbit");
    }

    #[test]
    fn test_filter_by_name_and_race() {
        let characters = create_test_characters();
        let filtered = filter_records(characters, Some("Legolas"), Some("Elf")).unwrap();
        assert_eq!(filtered.len(), 1);
        assert_eq!(filtered[0].name, "Legolas");
        assert_eq!(filtered[0].race, "Elf");
    }

    #[test]
    fn test_no_filters() {
        let characters = create_test_characters();
        let filtered = filter_records(characters, None, None).unwrap();
        assert_eq!(filtered.len(), 3); // 应返回所有角色
    }
}
