# Test Report for Week 8 Mini Project
> Puyang(Rivoc) Xu (px16)
## Overview

This test report provides a detailed overview of the unit testing conducted for Week 8 Mini Project - a command-line application designed to query a dataset of characters from "The Lord of the Rings" series based on specific filters such as name and race. The primary functionality tested includes filtering logic within the application, specifically the `filter_records` function, which is responsible for applying name and race filters to a list of character records.

## Test Environment

- **Programming Language**: Rust
- **Testing Framework**: Built-in Rust test framework (via `cargo test`)
- **Dependencies**: `csv`, `serde`, `clap`
- **Test Data**: A set of predefined character records representing notable figures from "The Lord of the Rings" series, including Frodo Baggins, Legolas, and Gimli.

## Test Cases

1. **Test Filter by Name**:
    - **Objective**: Verify that the application correctly filters character records by name.
    - **Input**: A name filter set to "Frodo".
    - **Expected Result**: Only the character record for Frodo Baggins is returned.
    - **Actual Result**: Passed. The filter correctly identified and returned only Frodo Baggins' record.

2. **Test Filter by Race**:
    - **Objective**: Ensure that the application can filter character records based on race.
    - **Input**: A race filter set to "Hobbit".
    - **Expected Result**: Only character records belonging to the "Hobbit" race are returned.
    - **Actual Result**: Passed. Only Frodo Baggins' record was returned, as expected.

3. **Test Filter by Name and Race**:
    - **Objective**: Test the application's ability to simultaneously filter character records by both name and race.
    - **Input**: Name filter set to "Legolas" and race filter set to "Elf".
    - **Expected Result**: Only the character record for Legolas is returned.
    - **Actual Result**: Passed. The application correctly identified and returned only the record for Legolas.

4. **Test No Filters**:
    - **Objective**: Confirm that the application returns all character records when no filters are applied.
    - **Input**: No name or race filters applied.
    - **Expected Result**: All character records are returned.
    - **Actual Result**: Passed. All predefined character records were returned.

## Conclusion

Week 8 Mini Project successfully passed all unit tests, demonstrating its capability to filter character records based on provided name and race criteria. The testing process confirmed that the application's filtering logic operates as expected under various conditions, including the use of individual filters and a combination thereof, as well as the absence of filters.
